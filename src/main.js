import Vue from 'vue'
import App from './App.vue'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import axios from 'axios'

// modify the base url from here
Vue.prototype.$http = axios
axios.defaults.baseURL = "http://localhost:8000"
axios.defaults.headers.common['header1'] = "Access-Control-Allow-Origin: *" // for all requests
axios.defaults.headers.common['header2'] = "Access-Control-Allow-Methods: POST, GET, OPTIONS" // for all requests
axios.defaults.headers.common['header3'] = "Access-Control-Allow-Headers: *" // for all requests

Vue.config.productionTip = false


new Vue({
  vuetify,
  render: h => h(App),
}).$mount('#app')
