import Vue from 'vue'
import '@mdi/font/css/materialdesignicons.css'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
    icons: {
        iconfont: 'mdi', // default - only for display purposes
    },
    themes: {
        theme: {
            primary: '#4caf50',
            secondary: '#ffeb3b',
            accent: '#9c27b0',
            error: '#f44336',
            warning: '#ffc107',
            info: '#cddc39',
            success: '#8bc34a'

        }
    }
}

export default new Vuetify({opts})